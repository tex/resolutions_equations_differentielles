\documentclass[a4paper,12pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[scaled]{helvet}
\usepackage[T1]{fontenc}
\usepackage[frenchb]{babel}

\usepackage{xcolor}
\usepackage{soul}
\usepackage{subcaption}	
\usepackage{graphicx}

\usepackage{lmodern}
\usepackage{ae,aecompl}	
\usepackage[widespace]{fourier} % pour math 'droite' : [upright]
\usepackage{mathrsfs} 	% font cali. \mathsc

\usepackage{amsfonts,amsmath,amssymb,amsthm,amscd}
\usepackage{nicefrac} % \nicefrac{a}{b} --> "a/b" but nicer
\usepackage[e]{esvect}  % \vv{arg} ou \vv*{arg}{indice}

\usepackage{array}
\usepackage{multicol}

\usepackage[left=1.5cm,right=1.5cm,top=2cm,bottom=2cm]{geometry}

\pagestyle{empty}

\setlength{\parindent}{0ex}

\newcommand{\diff}{\mathrm{d}}
\newcommand{\e}{\mathrm{e}}
\newcommand{\im}{\mathbf{i}}

\newcommand{\red}{\textcolor{red}}
\newcommand{\blue}{\textcolor{blue}}

\let\cosh\relax
\let\sinh\relax
\DeclareMathOperator{\cosh}{ch}
\DeclareMathOperator{\sinh}{sh}

\DeclareMathAlphabet{\pazocal}{OMS}{zplm}{m}{n}

\newtheorem*{remark}{Principe de superposition}

\setlength{\parindent}{0em}

\begin{document}

\begin{center}
	\begin{Large}
		\underline{\'Equations diff\'erentielles lin\'eaires résolues dans $\mathbb{K}=\mathbb{C}$ ou $\mathbb{R}$} \\
	\end{Large}
\end{center}

\[\large\fbox{$\red{\mathbf{y(t)=y_H(t)+y_P(t)}}$}\] \vspace{15pt}

\textbf{Équations différentielles linéaires du premier ordre :} \qquad $(\alpha\in\mathbb{K})$

\medskip

Du type \red{$y'(t)+r(t)\cdot y(t)=f(t)$} \\

\begin{center}


$\left\lbrace\begin{array}{l}

	\blue{y_H(t)=\alpha\cdot\e^{-R(t)}}\qquad \text{où }R'(t)=r(t) \\\\
	y_P(t)=\lambda(t)\cdot\e^{-R(t)}\quad\Rightarrow\quad\lambda'(t)=f(t)\cdot\e^{R(t)}

\end{array}\right.$
\end{center}

\bigskip

\textbf{Équations différentielles linéaires du premier ordre \underline{à coefficients constants} :} \qquad $(\alpha\in\mathbb{K})$

\medskip

Du type \red{$y'(t)+ry(t)=f(t)$} \\

\begin{center}


$\left\lbrace\begin{array}{ll}

	\text{Homogène :}& \blue{y_H(t)=\alpha\cdot\e^{-rt}} \\\\
	& \\
	\text{si } f(t)=P_n(t)\cdot\e^{\mu t}, & \left\lbrace\begin{array}{ll}
		\text{si }\mu\neq-r, & y_P(t)=Q_n(t)\cdot\e^{\mu t} \\\\
		\text{si }\mu=-r, & y_P(t)=t\cdot Q_n(t)\cdot\e^{-rt}=\overline{P_n}(t)\cdot\e^{-rt}
	\end{array}\right. \\\\
	& \\
	\text{si } f(t)=P(t)\cdot\cos(kt), & y_P(t)=\Re(y_\mathbb{C})\quad\text{ tel que } y_\mathbb{C}'+ry_\mathbb{C}=P(t)\cdot \e^{\im kt}\qquad(\mathbb{K}=\mathbb{R}) \\\\
	\text{si } f(t)=P(t)\cdot\sin(kt), & y_P(t)=\Im(y_\mathbb{C})\qquad(\mathbb{K}=\mathbb{R}) \\\\ & \\
	\text{si } f(t)=P(t)\cdot\cosh(kt), & y_P(t)=\dfrac{y_{+}+y_{-}}{2}\quad\text{ tel que } y_\pm'+ry_\pm=P(t)\cdot \e^{\pm kt}\qquad(\mathbb{K}=\mathbb{R}) \\\\
	\text{si } f(t)=P(t)\cdot\sinh(kt), & y_P(t)=\dfrac{y_{+}-y_{-}}{2}\qquad(\mathbb{K}=\mathbb{R}) \\\\ & \\
    \text{si }f(t)=K, & y_P(t)=\dfrac{K}{r}

\end{array}\right.$
\end{center}

\bigskip

\textit{Cas particuliers ($k\in\mathbb{K}$) :} \\ \begin{itemize}
	\item[$\bullet$] $f(t)=k\cdot\cos(t)\quad\Rightarrow\quad y_P(t)=k\cdot\left(\dfrac{r}{r^2+1}\cos(t)+\dfrac{1}{r^2+1}\sin(t)\right)$ \\\\
	\item[$\bullet$] $f(t)=k\cdot\sin(t)\quad\Rightarrow\quad y_P(t)=k\cdot\left(\dfrac{r}{r^2+1}\sin(t)-\dfrac{1}{r^2+1}\cos(t)\right)$
\end{itemize} 
		
\break

\textbf{Équations différentielles linéaires du second ordre \underline{à coefficients constants} :} \; $((\alpha,\beta)\in\mathbb{K}^2)$

\medskip

Du type \red{$y''(t)+py'(t)+qy(t)=f(t)$} \qquad Équation caractéristique : \fbox{$s^2+ps+q=0$} \\

\medskip

\begin{center}
$\left\lbrace\begin{array}{ll}

	\text{si }s_1\neq s_2, & \blue{y_H(t)=\alpha\cdot\e^{s_1t}+\beta\cdot\e^{s_2t}} \\\\
	\text{si }s_1=s_2, & \blue{y_H(t)=\e^{s_1t}\cdot(\alpha t+\beta)} \\\\
	& \\
	\text{si } f(t)=P_n(t)\cdot\e^{\mu t}, & \left\lbrace\begin{array}{ll}
		\text{si }\mu\neq s_1\neq s_2\neq\mu, & y_P(t)=Q_n(t)\cdot\e^{\mu t} \\\\
		\text{si }\mu=s_1\neq s_2, & y_P(t)=t\cdot Q_n(t)\cdot\e^{s_1t} \\\\
		\text{si }\mu=s_1=s_2, & y_P(t)=t^2\cdot Q_n(t)\cdot\e^{s_2t}=\overline{\overline{P_n}}(t)\cdot\e^{s_2t}
	\end{array}\right. \\\\
	& \\
	\text{si } f(t)=P(t)\cdot\cos(kt), & y_P(t)=\Re(y_\mathbb{C})\quad\text{ tel que } y_\mathbb{C}''+py_\mathbb{C}'+qy_\mathbb{C}=P(t)\cdot \e^{\im kt}\qquad(\mathbb{K}=\mathbb{R}) \\\\
	\text{si } f(t)=P(t)\cdot\sin(kt), & y_P(t)=\Im(y_\mathbb{C})\qquad(\mathbb{K}=\mathbb{R}) \\\\ & \\
	\text{si } f(t)=P(t)\cdot\cosh(kt), & y_P(t)=\dfrac{y_{+}+y_{-}}{2}\quad\text{ tel que } y_\pm''+py_\pm'+qy_\pm=P(t)\cdot \e^{\pm kt}\qquad(\mathbb{K}=\mathbb{R}) \\\\
	\text{si } f(t)=P(t)\cdot\sinh(kt), & y_P(t)=\dfrac{y_{+}-y_{-}}{2}\qquad(\mathbb{K}=\mathbb{R}) \\\\ & \\
    \text{si }f(t)=K, & y_P(t)=\dfrac{K}{q}


\end{array}\right.$
\end{center}

\bigskip

\textit{Cas des solutions réelles $(\mathbb{K}=\mathbb{R})$ :} en notant $s_i=a\pm\im b$ les solutions conjuguées de l'équation caractéristique dans le cas où $\Delta<0$, on a : \\
\[\blue{y_H(t)=\e^{at}\cdot(\alpha\cdot\cos(bt)+\beta\cdot\sin(bt))}\]

\medskip

\begin{remark}
Si $y_1$ est une solution particulière de $y'+a(x)y=b_1(x)$ et si $y_2$ est une solution particulière de $y'+a(x)y=b_2(x)$, alors $\lambda_1y_1+\lambda_2y_2$ est une solution particulière de $y'+a(x)y=\lambda_1b_1(x) +\lambda_2b_2(x)$, pour tous $\lambda_1, \lambda_2\in\mathbb{K}$. Ce principe s'applique de façon similaire aux équations différentielles de degré supérieur.
\end{remark}

\bigskip

\textit{Constantes :} les constantes $\alpha$ et $\beta$ sont à déterminer respectivement à l'aide des \textbf{conditions \break initiales} $y(0)$ et $y'(0)$. \\

\bigskip

\textit{Remarque : }\quad pour les équations dont le second membre est une fonction trigonométrique ou hyperbolique ($f(t)=\cos{(bt)}$ ou $\sin{(bt)}$ ou $\cosh{(bt)}$ ou $\sinh{(bt)}$), on préférera souvent poser :

$$\fbox{$y_P(t)=A\cdot\cos{(bt)}+B\cdot\sin{(bt)}\quad\text{ou}\quad y_P(t)=A\cdot\cosh{(bt)}+B\cdot\sinh{(bt)}$}$$

\bigskip
\bigskip

\textit{Notations : }\quad $y'(t)=\dfrac{\diff\,y(t)}{\diff t}$ \quad|\quad $y''(t)=\dfrac{\diff^2\,y(t)}{\diff t^2}$ \quad|\quad $\overline{P_n}(t)=\displaystyle\int P_n(t)\;\diff t$ \quad|\quad $\overline{\overline{P_n}}(t)=\displaystyle\iint P_n(t)\;\diff t^2$


\end{document}